package br.grandgabe.devopsback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "usuario", schema = "public")
public class Usuario {
    public Usuario(Usuario usuario) {
        this.username = usuario.getUsername();
        this.password = usuario.getPassword();
    }

    public Usuario() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Nullable
    private Long id;
    @Basic
    @NotNull
    @NotBlank
    @Size(min = 3, max = 20, message = "Username must be between 3 and 20 characters")
    private String username;
    @Basic
    @NotNull
    @Size(min = 8, message = "Password must be at least 8 character")
    private String password;
    @Email(message = "Email should be valid")
    private String email;
    @Nullable
    private Date created_at;

    private String perms;

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    private List<Card> cards;
}
