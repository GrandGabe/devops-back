package br.grandgabe.devopsback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "cards", schema = "public")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Basic
    @NotNull
    @Size(min = 3, max = 20, message = "Title must be between 3 and 20 characters")
    private String title;

    @Basic
    @NotNull
    @Size(min = 3, max = 100, message = "Description must be between 3 and 100 characters")
    private String description;

    @Basic
    @NotNull(message = "Card must have an image")
    private String image;

    @Basic
    private Date created_at;

    @NotNull(message = "Card must have a user")
    @ManyToOne
    @JoinColumn(name = "idusuario")
    private Usuario usuario;

}
