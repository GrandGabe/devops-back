package br.grandgabe.devopsback.dto;

import br.grandgabe.devopsback.model.Usuario;

public record UsuarioDTO(
        Long id,
        String username,
        String email,
        String created_at
) {
    public UsuarioDTO(Usuario usuario) {
        this(usuario.getId(), usuario.getUsername(), usuario.getEmail(), usuario.getCreated_at().toString());
    }
}
