package br.grandgabe.devopsback.dto;

import br.grandgabe.devopsback.model.Card;

public record CardDTO(Long id , String title, String description, String image, String created_at) {
    public CardDTO (Card card){
        this(card.getId(), card.getTitle(), card.getDescription(), card.getImage(), card.getCreated_at().toString());
    }
}
