package br.grandgabe.devopsback.controller;

import br.grandgabe.devopsback.dto.UsuarioDTO;
import br.grandgabe.devopsback.model.Usuario;
import br.grandgabe.devopsback.service.UsuarioService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    private UsuarioService service;

    public UsuarioController(UsuarioService service) {
        this.service = service;
    }
    @GetMapping
    public List<UsuarioDTO> getUsuarios() {
        return service.getUsuarios();
    }

    @GetMapping("/{id}")
    public UsuarioDTO getUsuario(@PathVariable Long id) {
        return service.getUsuario(id);
    }

    @PostMapping
    public UsuarioDTO createUsuario(@RequestBody@Valid Usuario usuario) {
        return service.createUsuario(usuario);
    }

}
