package br.grandgabe.devopsback.controller;

import br.grandgabe.devopsback.dto.CardDTO;
import br.grandgabe.devopsback.model.Card;
import br.grandgabe.devopsback.service.CardService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/card")
public class CardController {
    private CardService service;
    public CardController(CardService service) {
        this.service = service;
    }
    @GetMapping("/{id_usuario}")
    public List<CardDTO> getCardsByUsuarioId(@PathVariable Long id_usuario) {
        return service.getCardsByUsuarioId(id_usuario);
    }

    @PostMapping
    public CardDTO createCard(@RequestBody Card card) {
        return service.createCard(card);
    }
}
