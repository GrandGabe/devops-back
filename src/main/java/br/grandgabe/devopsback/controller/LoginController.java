package br.grandgabe.devopsback.controller;

import br.grandgabe.devopsback.model.Usuario;
import br.grandgabe.devopsback.service.TokenJWT;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {
    private final AuthenticationManager manager;

    private final TokenJWT tokenJWT;

    public LoginController(AuthenticationManager manager, TokenJWT tokenJWT) {
        this.manager = manager;
        this.tokenJWT = tokenJWT;
    }

    @PostMapping
    public ResponseEntity login(@RequestBody @Valid FieldsAuthentication usuario) {
        try{
            Authentication authentication = new UsernamePasswordAuthenticationToken(usuario.username(), usuario.password());
            Authentication at = manager.authenticate(authentication);

            User user = (User) at.getPrincipal();
            Usuario u = new Usuario();
            u.setUsername(user.getUsername());
            u.setPerms(user.getAuthorities().stream().toList().get(0).toString());
            String token = tokenJWT.genJwtToken(user);
            return ResponseEntity.ok().body(new DadosTokenJWT(token));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    private record DadosTokenJWT(String token){}
    private record FieldsAuthentication(String username, String password){}
}
