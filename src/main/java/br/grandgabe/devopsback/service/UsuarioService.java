package br.grandgabe.devopsback.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import br.grandgabe.devopsback.dto.UsuarioDTO;
import br.grandgabe.devopsback.model.Usuario;
import br.grandgabe.devopsback.repository.UsuarioRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UsuarioService {
    private UsuarioRepository repo;

    public UsuarioService(UsuarioRepository repo) {
        this.repo = repo;
    }

    public List<UsuarioDTO> getUsuarios() {
        List<UsuarioDTO> usuariosDTO = repo.findAll().stream().map(UsuarioDTO::new).toList();
        return !usuariosDTO.isEmpty() ? usuariosDTO : null;
    }

    public UsuarioDTO createUsuario(Usuario usuario) {
        usuario.setCreated_at(new Date());
        System.out.println(usuario.getPassword().length());
        usuario.setPassword(BCrypt.withDefaults().hashToString(12, usuario.getPassword().toCharArray()));
        System.out.println(usuario.getPassword().length());

        UsuarioDTO usuarioDTO = new UsuarioDTO(repo.save(usuario));
        return usuarioDTO != null ? usuarioDTO : null;
    }

    public UsuarioDTO getUsuario(Long id) {
        UsuarioDTO usuarioDTO = new UsuarioDTO(repo.findById(id).get());
        return usuarioDTO != null ? usuarioDTO : null;
    }
}
