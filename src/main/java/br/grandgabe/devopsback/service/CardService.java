package br.grandgabe.devopsback.service;

import br.grandgabe.devopsback.dto.CardDTO;
import br.grandgabe.devopsback.model.Card;
import br.grandgabe.devopsback.repository.CardRepository;
import jakarta.validation.Valid;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardService {
    private CardRepository repository;

    public CardService(CardRepository repository) {
        this.repository = repository;
    }

    public List<CardDTO> getCardsByUsuarioId(Long id) {
        return repository.findAllByUsuarioId(id).get().stream().map(CardDTO::new).collect(Collectors.toList());
    }

    public CardDTO createCard(@Valid Card card) {
        card.setCreated_at(new Date());
        return new CardDTO(repository.save(card));
    }
}
