package br.grandgabe.devopsback.service;

import br.grandgabe.devopsback.model.Usuario;
import br.grandgabe.devopsback.repository.UsuarioRepository;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

@Service
public class TokenJWT {

    private UsuarioRepository repo;

    public TokenJWT(UsuarioRepository repo) {
        this.repo = repo;
    }
    public String genJwtToken(User usuario){
        try{
            Algorithm algorithm = Algorithm.HMAC256("devops");
            return JWT.create()
                    .withIssuer("devops-back")
                    .withSubject(usuario.getUsername())
                    .withClaim("ROLE", usuario.getAuthorities().stream().toList().get(0).toString())
                    .withClaim("user_id", extractUserId(usuario))
                    .withExpiresAt(expirationDate())
                    .sign(algorithm);
        } catch (JWTCreationException e){
            throw new RuntimeException("Erro ao gerar token");
        }
    }

    private String extractUserId(User user) {
        Optional<Usuario> usuario = repo.findByUsername(user.getUsername());
        if (usuario.isEmpty()) {
            throw new RuntimeException("Usuário não encontrado");
        } else {
            return usuario.get().getId().toString();
        }
    }

    private Instant expirationDate(){
        return LocalDateTime.now().plusHours(2).toInstant(ZoneOffset.of("-03:00"));
    }

    public String getSubject(String token){
        try{
            Algorithm algorithm = Algorithm.HMAC256("devops");
            return JWT.require(algorithm)
                    .withIssuer("devops-back")
                    .build()
                    .verify(token)
                    .getSubject();
        }catch (JWTVerificationException e){
            throw new RuntimeException("Token inválido ou expirado");
        }
    }
}
