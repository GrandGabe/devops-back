package br.grandgabe.devopsback.service;

import br.grandgabe.devopsback.dto.UsuarioDTO;
import br.grandgabe.devopsback.model.Usuario;
import br.grandgabe.devopsback.repository.UsuarioRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService implements UserDetailsService {
    private UsuarioRepository repo;

    public LoginService(UsuarioRepository repo) {
        this.repo = repo;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Usuario> usuario = repo.findByUsername(username);

        if(usuario.isEmpty()){
            throw new UsernameNotFoundException("Usuário ou senha incorretos");
        } else {
            UserDetails user = User.withUsername(usuario.get().getUsername()).password(usuario.get().getPassword())
                    .authorities(usuario.get().getPerms())
                    .build();
            return user;
        }

    }
}
