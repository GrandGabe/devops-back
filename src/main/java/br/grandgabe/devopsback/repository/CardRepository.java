package br.grandgabe.devopsback.repository;

import br.grandgabe.devopsback.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CardRepository extends JpaRepository<Card, Long> {
    Optional<List<Card>> findAllByUsuarioId(Long idUsuario);
}
