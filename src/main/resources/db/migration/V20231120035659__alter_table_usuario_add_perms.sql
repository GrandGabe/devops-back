ALTER TABLE usuario ADD COLUMN IF NOT EXISTS perms text;

UPDATE usuario SET perms = 'user' WHERE perms is null;

ALTER TABLE usuario ALTER COLUMN perms SET NOT NULL;
