CREATE TABLE IF NOT EXISTS cards (
    id BIGSERIAL PRIMARY KEY,
    title TEXT NOT NULL,
    description TEXT NOT NULL,
    image TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    idusuario BIGINT NOT NULL,
    FOREIGN KEY (idusuario) REFERENCES usuario(id)
);
