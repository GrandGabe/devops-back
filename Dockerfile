FROM openjdk:21 as build

WORKDIR /workspace/app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src


RUN --mount=type=cache,target=/root/.m2 ./mvnw install -DskipTests

ARG MVNARGS="package"
RUN ./mvnw -B ${MVNARGS} -DskipTests

FROM openjdk:21
VOLUME /workspace

COPY --from=build /workspace/app/target/*.jar app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/urandom","-jar","/app.jar"]
